const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

app.use('/static', express.static(path.join(__dirname, 'public')));
app.use('/', (req, res) => {
    res.send('Hello from express');
});

app.listen(port, () => process.stderr.write(`Listening on port ${port}\n`));
