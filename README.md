
This takes a simple express app.

* Builds it in a container during the first CI stage
* Tests it as a service and image in the second stage
* Tags the resulting image as `latest` if the build has a git tag

